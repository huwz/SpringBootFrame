package ac.sict.test;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test(){
        redisTemplate.opsForValue().set("str","yulong");
        redisTemplate.boundValueOps("str").get();
        System.out.println("str="+redisTemplate.opsForValue().get("str"));
        redisTemplate.boundHashOps("h_key").put("name","yulong");
        redisTemplate.boundHashOps("h_key").put("age",13);
        System.out.println("hash散列的所有域:"+redisTemplate.boundHashOps("h_key").keys());
        System.out.println("hash散列的所有域的值:"+redisTemplate.boundHashOps("h_key").values());
        redisTemplate.boundListOps("1_key").leftPush("c");
        redisTemplate.boundListOps("1_key").leftPush("b");
        redisTemplate.boundListOps("1_key").leftPush("a");
        System.out.println("list列表中的所有元素:"+redisTemplate.boundListOps("1_key").range(0,-1));
        redisTemplate.boundSetOps("s_key").add("a","b","c");
        System.out.println("set集合中的所有元素:"+redisTemplate.boundSetOps("s_key").members());
        redisTemplate.boundZSetOps("z_key").add("a",30);
        redisTemplate.boundZSetOps("z_key").add("b",20);
        redisTemplate.boundZSetOps("z_key").add("c",10);
        System.out.println("zset有序集合中的所有元素:"+redisTemplate.boundZSetOps("z_key").range(0,-1));
    }
}
